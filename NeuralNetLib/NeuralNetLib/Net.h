#pragma once
#include <iostream>
#include <vector>
#include "Neuron.h"
#include <cassert>

//needs a vector of doubles of which contain the topology, the input values, and the output values of the neural net

using std::vector;
using std::endl;
using std::cout;

class Neuron;



class Net
{
public:
	//topology size is amount of layers and value is how many neurons in each layer
	Net(const vector<unsigned int> &topology);
	~Net();

	void feedForward(const vector<double> &inputVals);
	void backProp(const vector<double> &targetVals);
	vector<double> getResults(vector<double> resultVals);

private:
	vector<Layer> m_layers; //m_layers[layerNum][neuronNum]
	double m_error;
	double m_recentAverageError; 
	double m_recentAverageSmoothingFactor;
};

