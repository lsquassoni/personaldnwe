#include "Net.h"
#include<iostream>
using std::cout;
using std::endl;



Net::Net(const vector<unsigned int> &topology)
{
	m_error = 0;
	unsigned int numLayers = topology.size();

	for (unsigned int i = 0; i < numLayers; i++) {
		//create new layer1
		m_layers.push_back(Layer());
		unsigned int numOutputs = i == topology.size() - 1 ? 0 : topology[i+1];
		//fill the layer with neurons
		//add a bias neuron to the layer
		for (unsigned int j = 0; j <= topology[i]; j++) {
			m_layers.back().push_back(Neuron(numOutputs, j));
			cout << "Neuron made" << endl;
		}
		m_layers.back().back().setOutputVal(0.0000001);
	}
}


Net::~Net()
{
}

void Net::feedForward(const vector<double>& inputVals)
{

	for (unsigned int i = 0; i < inputVals.size(); i++) {
		m_layers[0][i].setOutputVal(inputVals[i]);
	}

	//forward propagate
	for (unsigned int i = 1; i < m_layers.size(); i++) {
		Layer &prevLayer = m_layers[i - 1];
		for (unsigned int j = 0; j < m_layers[i].size() - 1; j++) {
			m_layers[i][j].feedForward(prevLayer);
		}
	}
}

void Net::backProp(const vector<double>& targetVals)
{
	//Calculate overall net error (RMS of output neuron errors)

	Layer &outputLayer = m_layers.back();
	//error is the error that the neuron is in
	m_error = 0; 
	for (unsigned int i = 0; i < outputLayer.size() - 1; i++) {
		double delta = targetVals[i] - outputLayer[i].getOutputVal();
		m_error += delta * delta;
	}
	m_error /= outputLayer.size() - 1;
	m_error = sqrt(m_error); //rms
	
	// Impliment a recent average measurement

	m_recentAverageError = (m_recentAverageError * m_recentAverageSmoothingFactor + m_error) / (m_recentAverageSmoothingFactor + 1.0);

	//Calculate gradients on hidden layers
	for (unsigned int i = 0; i < outputLayer.size() - 1; i++) {
		outputLayer[i].calcOutputGradients(targetVals[i]);
	}

	for (unsigned int layerNum = m_layers.size() - 2; layerNum > 0; --layerNum) {
		Layer & hiddenLayer = m_layers[layerNum];
		Layer & nextLayer = m_layers[layerNum + 1]; 

		for (unsigned int i = 0; i < hiddenLayer.size(); i++) {
			hiddenLayer[i].calcHiddenGradients(nextLayer);
		}
	}
	

	//For all layers from outputs to first hidden layer
	//update connection weights
	for (unsigned int i = m_layers.size() - 1; i > 0; --i) {
		Layer & layer = m_layers[i];
		Layer & prevLayer = m_layers[i - 1];

		for (unsigned int j = 0; j < layer.size()-1; j++) {
			layer[j].updateInputWeights(prevLayer);
		}
	}

}

vector<double> Net::getResults(vector<double> resultVals) 
{
	resultVals.clear();

	for (unsigned int i = 0; i < m_layers.back().size() - 1; i++) {
		resultVals.push_back(m_layers.back()[i].getOutputVal());
	}
	return resultVals;
}
