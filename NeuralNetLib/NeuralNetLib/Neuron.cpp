#include "Neuron.h"


double Neuron::eta = 0.15;
double Neuron::alpha = 0.5;

Neuron::Neuron(unsigned int numOutputs, unsigned int myIndex)
{
	for (unsigned int i = 0; i < numOutputs; ++i) {
		m_outputWeights.push_back(Connection());
		m_outputWeights.back().weight = randomWeight();
	}
	m_myIndex = myIndex;
}


Neuron::~Neuron()
{
}

void Neuron::setOutputVal(double val)
{
	m_outputVal = val;
}

double Neuron::getOutputVal() const
{
	return m_outputVal;
}

void Neuron::feedForward(const Layer & prevLayer)
{
	double sum = 0.0;

	//sum the previous layers outputs (which are our inputs)
	//include the bias node from the previous layer

	for (unsigned int i = 0; i < prevLayer.size(); i++) {
		sum += prevLayer[i].getOutputVal() * prevLayer[i].m_outputWeights[m_myIndex].weight;
	}

	m_outputVal = Neuron::activationFunction(sum);
}

void Neuron::calcOutputGradients(double targetVal)
{
	double delta = targetVal - m_outputVal;
	m_gradient = delta * Neuron::activationFunctionDerivative(m_outputVal);
}

void Neuron::calcHiddenGradients(const Layer & nextLayer)
{
	double dow = sumDOW(nextLayer);
	m_gradient = dow * Neuron::activationFunction(m_outputVal);
}

double Neuron::sumDOW(const Layer & nextLayer)
{
	double sum = 0;

	for (unsigned int i = 0; i < nextLayer.size() - 1; i++) {
		sum += m_outputWeights[i].weight * nextLayer[i].m_gradient;
	}

	return sum;
}

void Neuron::updateInputWeights(Layer & prevLayer)
{
	//weights to be updated are in the connection container for each neuron int the neurons preceding layer


	for (unsigned int i = 0; i < prevLayer.size(); i++) {
		Neuron & neuron = prevLayer[i];
		
		double oldDeltaWeight = neuron.m_outputWeights[m_myIndex].deltaWeight;

		// change is created by, Individual input, magnified by the gradient train rate
		// the momentum which is a fraction of the previous delta weight
		double newDeltaWeight = eta * neuron.getOutputVal() *m_gradient + alpha * oldDeltaWeight;
			
		neuron.m_outputWeights[m_myIndex].deltaWeight = newDeltaWeight;
		neuron.m_outputWeights[m_myIndex].weight += newDeltaWeight;
	}
}


double Neuron::randomWeight()
{
	return rand() / double(RAND_MAX);
}

double Neuron::activationFunction(double sum)
{
	//return tanh(sum);
	return sum / 1 / (1 + exp(-sum));
}

double Neuron::activationFunctionDerivative(double sum)
{
	return sum * (1 - sum);
}
