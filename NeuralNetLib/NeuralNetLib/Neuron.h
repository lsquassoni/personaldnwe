#pragma once
#include <vector>
#include <cstdlib>
#include <cmath>

using std::vector;

class Neuron;

typedef vector<Neuron> Layer;

struct Connection {
	double weight;
	double deltaWeight;
};

class Neuron
{
public:
	Neuron(unsigned int numOutputs, unsigned int myIndex);
	~Neuron();

	void setOutputVal(double val);
	double getOutputVal() const;

	void feedForward(const Layer &prevLayer);

	void calcOutputGradients(double targetVal);
	void calcHiddenGradients(const Layer & nextLayer); 
	double sumDOW(const Layer & nextLayer);
	void updateInputWeights(Layer &prevLayer);


private:
	static double randomWeight();
	static double activationFunction(double sum);
	static double activationFunctionDerivative(double sum);
	double m_outputVal; //output val of the neuron
	vector<Connection> m_outputWeights;
	unsigned int m_myIndex;
	double m_gradient;
	static double eta;  //[0...1] overall net training rate
	static double alpha; //[0.....n]
};

