#pragma once
#include "Simulation.h"
#include <vector>

// A Genetic Algorithm for playing a super simple game
// More for demonstrating the training process than actually playing

enum orgStats {
	HP = 1,
	ATTACKSPEED,
	PHYSICALDAMAGE,
	MAGICALDAMAGE,
	PHYSICALARMOR,
	MAGICALARMOR
};

class GAPlayer
{
public:

	struct organism {
		std::vector<orgStats> stats;
		Stats genome;
		int fitness;

		bool isAlive = true;

		bool operator<(const organism& a) const
		{
			return fitness < a.fitness;
		}
	};

	GAPlayer();
	~GAPlayer();

	std::vector<orgStats> AssignFreshStats();
	void PopulateGenome(organism& temp);
	Stats Train();
	int CalculateFitness(Stats org);

protected:
	void FreshPopulation();
	int Fight(Stats &attacker, Stats &defender);
	void GenerationReport(organism best);
	void Breed(const std::vector<organism> &prevGeneration);
	void Mutate(organism& org);

	orgStats GenerateRandNum(int min, int max);

	const int maxPopSize = 200;
	const int generations = 50;
	int currentGeneration = 0;
	std::vector<organism> population;
	Stats currentGenome;
	organism tempSubject;
	
};

