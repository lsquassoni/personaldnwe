#pragma once

// The Game Class runs the game world
// It is able to return information about the current game state
// It is able to advance the game to the next state using the player's move

class GAPlayer;

class Game
{
public:
	struct pos {
		int x, y;
	};

	enum dir{UP, DOWN, LEFT, RIGHT, NUM_DIR};

	Game();
	~Game();

	pos move(dir moveDir);
	pos playout(GAPlayer* player, bool verbose); // play a series of moves from a player
	pos getStart() { return start; }
	pos getGoal() { return goal; }
	pos getCurrent() { return current; }
	int getWidth() { return width; }
	int getHeight() { return height; }
	int distToGoal(pos loc); // manhattan distance to the goal

protected:
	/*	8| | | | | | | | |g|
		7| | | | | | | | | |
		6| | | | | | | | | |
		5| | | | | | | | | |
		4| | | | |s| | | | | Our Game World is a simple
		3| | | | | | | | | | 9 x 9 grid of squares
		2| | | | | | | | | |
		1| | | | | | | | | | By default, we start at 4,4
		0| | | | | | | | | | and try to get to 8,8
		 |0|1|2|3|4|5|6|7|8|
	*/
	static const int width = 9, height = 9; // default size for now
	pos start;
	pos goal;
	pos current;
};

