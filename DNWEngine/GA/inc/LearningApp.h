#pragma once
#include "Game.h"

class GAPlayer;

class LearningApp
{
public:
	LearningApp();
	~LearningApp();

	Game::pos run();
	void train();

protected:
	Game* game;
	GAPlayer* player;

};

