#include "GAPlayer.h"
#include <iostream>
#include <time.h>
#include <algorithm>

GAPlayer::GAPlayer()
{
	time_t t;
	srand(time(&t));

	tempSubject.stats = AssignFreshStats();
	PopulateGenome(tempSubject);
}


GAPlayer::~GAPlayer()
{
}

std::vector<orgStats> GAPlayer::AssignFreshStats()
{
	organism temp;
	temp.stats.resize(30);
	for (size_t i = 0; i < temp.stats.size(); i++)
	{
		temp.stats[i] = GenerateRandNum(1, 6);
	}
	
	return temp.stats;
}

void GAPlayer::PopulateGenome(organism& temp)
{
	for (size_t i = 0; i < temp.stats.size(); i++)
	{
		//temp.stats[i]
		switch (temp.stats[i])
		{
		case HP:
			temp.genome.hp++;
			break;
		case ATTACKSPEED:
			temp.genome.attackSpeed++;
			break;
		case PHYSICALDAMAGE:
			temp.genome.physicalDamage++;
			break;
		case MAGICALDAMAGE:
			temp.genome.magicalDamage++;
			break;
		case PHYSICALARMOR:
			temp.genome.physicalArmor++;
			break;
		case MAGICALARMOR:
			temp.genome.magicalArmor++;
			break;
		}
	}
	temp.genome.attackSpeed /= 3;
}

Stats GAPlayer::Train()
{
	Stats testSubject;
	// initialise the population
	FreshPopulation();

	organism best;

	// Run multiple Generations
	for (size_t g = 0; g < generations; g++)
	{
		currentGeneration = g + 1;
		// run every member of the population and receive the results so we can visualise it
		for (size_t i = 0; i < population.size(); i++)
		{
			testSubject = tempSubject.genome;
			// run the organism through the entire game and find the end position
			currentGenome = population[i].genome;
			int rounds = Fight(currentGenome, testSubject);
			
			if (currentGenome.hp <= 0)
				population[i].isAlive = false;

			population[i].fitness = CalculateFitness(currentGenome) * rounds;
		}

		// sort the population based on fitness
		std::sort(population.begin(), population.end());
		
		// grab the best one from this population and save it
		best = population[population.size() - 1];

		GenerationReport(best);

		// cull the herd (just the lower half of them)
		population.erase(population.begin(), population.begin() + population.size() / 2);

		// breed some more to fill their places, replace old population with a new one
		std::vector<organism> oldPop = population;
		Breed(oldPop);
	}

	return best.genome;
}

int GAPlayer::CalculateFitness(Stats org)
{
	organism temp;
	if (org.hp > 0)
		temp.fitness = 1 + org.hp;
	else
		temp.fitness = 0;
	
	return temp.fitness;
}

void GAPlayer::FreshPopulation()
{
	for (size_t i = 0; i < maxPopSize; i++)
	{
		organism citizen;
		citizen.stats = AssignFreshStats();
		PopulateGenome(citizen);
		population.push_back(citizen);
	}
}

int GAPlayer::Fight(Stats &attacker, Stats &defender)
{
	int rounds = 0;
	while (attacker.hp > 0 && defender.hp > 0) {
		if (rounds >= 50)
		{
			rounds = 0;
			break;
		}
		if (attacker.attackSpeed > defender.attackSpeed) {
			Simulation::attack(attacker, defender);
			Simulation::attack(defender, attacker);
			rounds++;
		}
		else if (attacker.attackSpeed < defender.attackSpeed) {
			Simulation::attack(defender, attacker);
			Simulation::attack(attacker, defender);
			rounds++;
		}
		else {
			srand(time(NULL));
			int coin = rand() % 2;
			if (coin == 1) {
				Simulation::attack(attacker, defender);
				Simulation::attack(defender, attacker);
				rounds++;
			}
			else {
				Simulation::attack(defender, attacker);
				Simulation::attack(attacker, defender);
				rounds++;
			}
		}
	}
	return rounds;
}

void GAPlayer::GenerationReport(organism best)
{
	std::cout << "\nGeneration: " << currentGeneration << std::endl;
	std::cout << "Best Organism: " << std::endl;
	std::cout << "HP: " << best.genome.hp << std::endl;
	std::cout << "Attack Speed: " << best.genome.attackSpeed << std::endl;
	std::cout << "Physical Damage: " << best.genome.physicalDamage << std::endl;
	std::cout << "Magical Damage: " << best.genome.magicalDamage << std::endl;
	std::cout << "Physical Armor: " << best.genome.physicalArmor << std::endl;
	std::cout << "Magical Armor: " << best.genome.magicalArmor << std::endl;
	best.isAlive ? std::cout << "Won" << std::endl : std::cout << "Lost" << std::endl;
}

void GAPlayer::Breed(const std::vector<organism> &prevGeneration)
{
	// clear current population
	population.clear();

	// possible breeding strategies:
	// 1. Breed pairs with a split point
	// 2. Breed pairs with multiple splits
	// 3. Breed in more than pairs

	for (size_t i = 0; i < prevGeneration.size(); i++)
	{
		// create two pairings for every organism
		int j = rand() % prevGeneration.size();
		while (i == j)
		{	// make sure that i and j aren't the same
			j = rand() % prevGeneration.size();
		}
		int k = rand() % prevGeneration.size();
		while (i == k || j == k)
		{	// make sure that the pairing isn't the same as previous
			k = rand() % prevGeneration.size();
		}

		// Pairs with a single split point
		organism newborn;
		newborn.fitness = 0;

		bool split = rand() % 2;
		for (size_t i = 0; i < 30; i++)
		{
			split = rand() % 2;
			split ? newborn.stats.push_back(prevGeneration[j].stats[i]) : newborn.stats.push_back(prevGeneration[k].stats[i]);
		}
		//Mutate(newborn);
		PopulateGenome(newborn);
		population.push_back(newborn);
				

		organism newborn2;
		newborn2.fitness = 0;

		split = rand() % 2;
		for (size_t i = 0; i < 30; i++)
		{
			split = rand() % 2;
			split ? newborn2.stats.push_back(prevGeneration[j].stats[i]) : newborn2.stats.push_back(prevGeneration[k].stats[i]);
		}
		//Mutate(newborn2);
		PopulateGenome(newborn2);
		population.push_back(newborn2);
	}

}

void GAPlayer::Mutate(organism & org)
{
	int mutateStat = rand() % 6;
	switch (mutateStat)
	{
	case 0:
		org.genome.hp += rand() % 3 + 1;
		break;
	case 1:
		org.genome.attackSpeed += rand() % 3 + 1;
		break;
	case 2:
		org.genome.physicalDamage += rand() % 3 + 1;
		break;
	case 3:
		org.genome.magicalDamage += rand() % 3 + 1;
		break;
	case 4:
		org.genome.physicalArmor += rand() % 3 + 1;
		break;
	case 5:
		org.genome.magicalArmor += rand() % 3 + 1;
		break;
	default:
		break;
	}
}

orgStats GAPlayer::GenerateRandNum(int min, int max)
{
	return orgStats(rand() % max + min); 
}

