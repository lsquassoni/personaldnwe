#include "Simulation.h"
#include <functional>
using std::vector;

void main() 
{
	Sim sim;
	
	GAlgorithm Civ(35, 1000, 50, 1, 6, &sim);
	while (true)
	{
		system("CLS");
		sim.setMode();
		Civ.FreshPopulation();
		Civ.Train();
		system("pause");
	}
	
}