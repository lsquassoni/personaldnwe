#include "Simulation.h"

using std::cout;
using std::endl;
using std::cin;


Sim::Sim()
{
}


Sim::~Sim()
{
}

void Sim::assignPoints()
{
	while (neuralKing.availablePoints > 0) {
		neuralKing.availablePoints -= statCost.hpCost * 10;
		statsPurchased.hpAdd = 10;
		neuralKing.hp = statsPurchased.hpAdd * statAmountToMultiply.hpAdd;

		neuralKing.availablePoints -= statCost.atkSpeedCost * 3;
		statsPurchased.atkSpeedAdd = 3;
		neuralKing.attackSpeed = statsPurchased.atkSpeedAdd * statAmountToMultiply.atkSpeedAdd;

		neuralKing.physicalDamage -= statCost.pDamageCost * 1;
		statsPurchased.pDamageAdd = 1;
		neuralKing.physicalDamage = statsPurchased.pDamageAdd * statAmountToMultiply.pDamageAdd;

		neuralKing.availablePoints -= statCost.mDamageCost * 1;
		statsPurchased.mDamageAdd = 1;
		neuralKing.magicalDamage = statsPurchased.mDamageAdd * statAmountToMultiply.mDamageAdd;

		neuralKing.availablePoints -= statCost.pArmorCost * 8;
		statsPurchased.pArmorAdd = 8;
		neuralKing.physicalArmor = statsPurchased.pArmorAdd * statAmountToMultiply.pArmorAdd;

		neuralKing.availablePoints -= statCost.mArmorCost * 6;
		statsPurchased.mArmorAdd = 6;
		neuralKing.magicalArmor = statsPurchased.mArmorAdd * statAmountToMultiply.mArmorAdd;
	}
}

int Sim::fight()
{
	int rounds = 0;
	while (neuralKing.hp > 0 && geneWilder.hp > 0) {
		rounds++;
		if (neuralKing.attackSpeed > geneWilder.attackSpeed) {
			attack(neuralKing, geneWilder);
			attack(geneWilder, neuralKing);
		}
		else if (neuralKing.attackSpeed < geneWilder.attackSpeed) {
			attack(geneWilder, neuralKing);
			attack(neuralKing, geneWilder);
		}
		else {
			srand((unsigned int)time(NULL));
			int coin = rand() % 2;
			if (coin == 1) {
				attack(neuralKing, geneWilder);
				attack(geneWilder, neuralKing);
			}
			else {
				attack(geneWilder, neuralKing);
				attack(neuralKing, geneWilder);
			}
		}
		if (rounds > 50)
			break;
	}
	if (neuralKing.hp < 0)
		neuralKing.hp = 0;
	else if (geneWilder.hp < 0)
		geneWilder.hp = 0;
	return rounds;
}

void Sim::setMode()
{
	int mode;
	cout << "Select Game Mode:" << endl;
	cout << "-----------------------" << endl;
	cout << "0. Basic\n1. Healthy Mode\n2. Buff Mode\n3. Strange Mode\n4. Weird Mode\n5. Custom Mode" << endl;
	cout << "-----------------------\n" << (char)62;
	cin >> mode;
	

	switch (mode) {
	case BASE:
		setStatRatio(3,1,2,2,1,1);
		break;
	case HEALTHYMODE:
		setStatRatio(5,1,2,2,1,1);
		break;
	case BUFFMODE: 
		setStatRatio(4,1,3,2,1,1);
		break;
	case STRANGEMODE:
		setStatRatio(4,1,2,3,1,1);
		break;
	case WEIRDMODE:
		setStatRatio(7,2,7,5,3,7);
		break;
	case CUSTOM:
		customStatRatio();
		break;
	}
}

void Sim::setStatRatio(int hp, int attackSpeed, int pDamage, int mDamage, int pArmor, int mArmor)
{
	statAmountToMultiply.hpAdd = hp;
	statAmountToMultiply.atkSpeedAdd = attackSpeed;
	statAmountToMultiply.pDamageAdd = pDamage;
	statAmountToMultiply.mDamageAdd = mDamage;
	statAmountToMultiply.pArmorAdd = pArmor;
	statAmountToMultiply.mArmorAdd = mArmor;
}

void Sim::customStatRatio()
{
	int temp;

	system("cls");	
	cout << "Input HP point value: ";
	cin >> temp;
	statAmountToMultiply.hpAdd = temp;

	system("cls");
	cout << "Input attack speed point value: ";
	cin >> temp;
	statAmountToMultiply.atkSpeedAdd = temp;

	system("cls");
	cout << "Input physical damage point value: ";
	cin >> temp;
	statAmountToMultiply.pDamageAdd = temp;

	system("cls");
	cout << "Input magic damage point value: ";
	cin >> temp;
	statAmountToMultiply.mDamageAdd = temp;

	system("cls");
	cout << "Input physical armor point value: ";
	cin >> temp;
	statAmountToMultiply.pArmorAdd = temp;

	system("cls");
	cout << "Input magic armor point value: ";
	cin >> temp;
	statAmountToMultiply.mArmorAdd = temp;

	system("cls");
	cout << "Summary:\n\nHP: " << statAmountToMultiply.hpAdd << "\nAttack Speed: " << statAmountToMultiply.atkSpeedAdd << "\nPhysical Damage: " << statAmountToMultiply.pDamageAdd << "\nMagic Damage: " << statAmountToMultiply.mDamageAdd
		<< "\nPhysical Armor: " << statAmountToMultiply.pArmorAdd << "\nMagic Armor: " << statAmountToMultiply.mArmorAdd << "\n"<< endl;
}



void Sim::resetGame()
{
	geneWilder.availablePoints = maxPoints;
	neuralKing.availablePoints = maxPoints;
}

void Sim::attack(Stats & attacker, Stats & defender)
{
	for (int i = 0; i <= attacker.attackSpeed; i++) {
		if (attacker.hp > 0) {
			if (defender.physicalArmor < attacker.physicalDamage) {
				defender.hp -= attacker.physicalDamage - defender.physicalArmor;
			}
			if(defender.magicalArmor < attacker.magicalDamage){
				defender.hp -= attacker.magicalDamage - defender.magicalArmor;
			}
		}
	}
}

Stats Sim::ConvertToStats(std::vector<int> genome)
{
	Stats temp;
	for (size_t i = 0; i < genome.size(); i++)
	{
		switch (genome[i])
		{
		case 1:temp.hp++; break;
		case 2:temp.attackSpeed++; break;
		case 3:temp.physicalDamage++; break;
		case 4:temp.magicalDamage++; break;
		case 5:temp.physicalArmor++; break;
		case 6:temp.magicalArmor++; break;
		}
	}
	temp.hp *= 3;
	temp.attackSpeed /= 3;
	return temp;
}

void Sim::SetGene(Stats replacement)
{
	geneWilder = replacement;
}

int Sim::Fitness(std::vector<int> genome)
{
	resetGame();
	assignPoints();
	int neuralKingHP = neuralKing.hp;
	geneWilder = ConvertToStats(genome);
	int TotalHealth = geneWilder.hp;
	int rounds = fight();
	//r(T-C) / Dd	
	if (neuralKing.hp == neuralKingHP)
		return rounds * (TotalHealth - geneWilder.hp);
	else
		return rounds * (TotalHealth - geneWilder.hp) / (neuralKingHP - neuralKing.hp);
}

void Sim::GenerationReport(std::vector<int> genome, int fitness, int generation)
{
	Stats temp;
	for (size_t i = 0; i < genome.size(); i++)
	{
		switch (genome[i])
		{
		case 1:temp.hp++; break;
		case 2:temp.attackSpeed++; break;
		case 3:temp.physicalDamage++; break;
		case 4:temp.magicalDamage++; break;
		case 5:temp.physicalArmor++; break;
		case 6:temp.magicalArmor++; break;
		}
	}
	temp.hp *= 3;
	temp.attackSpeed /= 3;

	std::cout << "Generation " << generation << " best Genome: " << std::endl;
	cout << "Summary:\n\nHP: " << temp.hp << "\nAttack Speed: " << temp.attackSpeed << "\nPhysical Damage: " << temp.physicalDamage << "\nMagic Damage: " << temp.magicalDamage
		<< "\nPhysical Armor: " << temp.physicalArmor << "\nMagic Armor: " << temp.magicalArmor << "\nFitness: " << fitness << "\n" << endl;
}

