#pragma once
#include<iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <GAlgorithm.h>

struct Stats
{
	int hp = 0;
	int attackSpeed = 0;
	int physicalDamage = 0;
	int magicalDamage = 0;
	int physicalArmor = 0;
	int magicalArmor = 0;
	int availablePoints = 0;
};

struct Statbuy {
	int hpCost=1;
	int atkSpeedCost=3;
	int pDamageCost=1;
	int mDamageCost=1;
	int pArmorCost=1;
	int mArmorCost=1;
};

struct StatAdd {
	int hpAdd;
	int atkSpeedAdd;
	int pDamageAdd;
	int mDamageAdd;
	int pArmorAdd;
	int mArmorAdd;
};

enum Modes
{
	BASE, HEALTHYMODE, BUFFMODE, STRANGEMODE, WEIRDMODE, CUSTOM
};

class Sim : public Simulation
{
public:
	Sim();
	~Sim();
	
	virtual void assignPoints();
	int fight();
	void setMode();
	void setStatRatio(int hp, int attackSpeed, int pDamage, int mDamage, int pArmor, int mArmor);
	void customStatRatio();
	void resetGame();
	static void attack(Stats & attacker, Stats & defender);

	Stats ConvertToStats(std::vector<int> genome);

	void SetGene(Stats replacement);


	int Fitness(std::vector<int> genome);
	void GenerationReport(std::vector<int>genome, int fitness, int generation);
private:
	const int maxPoints = 35;
	Stats geneWilder;
	Stats neuralKing;
	const Statbuy statCost; // The cost of buying a stat
	StatAdd statAmountToMultiply; // The number that your stat goes up by when the purchase is made
	StatAdd statsPurchased; // this is the way the available points have been allocated 
};

